# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/irisdesabille/package-registry/compare/v1.0.6...v1.1.0) (2021-08-23)


### Features

* new text ([f7f512b](https://gitlab.com/irisdesabille/package-registry/commit/f7f512bd248b0b41b0372cb2853fff909a0f8509))

### [1.0.6](https://gitlab.com/irisdesabille/package-registry/compare/v1.0.5...v1.0.6) (2021-08-23)

### [1.0.5](https://gitlab.com/irisdesabille/package-registry/compare/v1.0.4...v1.0.5) (2021-08-23)

### [1.0.4](https://gitlab.com/irisdesabille/package-registry/compare/v1.0.3...v1.0.4) (2021-08-23)

### [1.0.3](https://gitlab.com/irisdesabille/package-registry/compare/v1.0.2...v1.0.3) (2021-08-23)

### [1.0.2](https://gitlab.com/irisdesabille/package-registry/compare/v1.0.1...v1.0.2) (2021-08-23)

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
