import { defineConfig } from 'vite'
import { createVuePlugin } from 'vite-plugin-vue2'
import visualizer from 'rollup-plugin-visualizer'


const path = require('path')

// https://vitejs.dev/config/
export default defineConfig({
    resolve: {
        alias: [
            {
                find: '@components',
                replacement: path.resolve(__dirname, 'components')
            }
        ]
    },
    build: {
        lib: {
            entry: path.resolve(__dirname, 'components/index.js'),
            name: 'vue-generics'
        },
        rollupOptions: {
            external: ['vue'], // deps that shouldn't be bundled
            input: {
                main: path.resolve(__dirname, 'components/index.js')
            },
            output: {
                // Provide global variables to use in the UMD build
                // for externalized deps
                globals: {
                    vue: 'Vue'
                }
            }
        }
    },
    plugins: [
        createVuePlugin({
            style: {
                scoped: true,
                preprocessLang: 'scss'
            }
        }),
        visualizer({
            template: "sunburst"
        })
    ]
})